import image from './asset/images/48.jpg';

function App() {
  return (
    <div>
      <div style={{width: "1000px", margin: "0 auto", textAlign: "center", border: "1px solid #ddd", marginTop: "100px", backgroundColor: "bisque", paddingBottom: "50px"}}>
        <div style={{marginTop: "-50px"}}>
          <img src={image} alt='image user' style={{width: "100px", borderRadius: "50%"}}></img>
        </div>
        <div>
          <p style={{fontStyle: "italic"}}>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div style={{fontSize: "12px"}}>
          <b style={{color: "blue"}}>Tammy Stevens</b>
          <b style={{color: "red"}}> * Front End Developer</b>
        </div>
      </div>
    </div>
  );
}

export default App;
